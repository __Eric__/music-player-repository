package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    void onUserSelectedSongAtPosition(int position){
        switchSong(currentSongIndex, position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateDataModel();
        connectXMLViews();
        setupRecyclerView();
        displayCurrentSong();
        setupButtonHandlers();
    }

    void populateDataModel(){
        playlist.name = "My Playlist";
        playlist.songs = new ArrayList<Song>();

        Song song = new Song();
        song.songName = "Jazz Comedy";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.jazzcomedy;
        song.mp3Resource = R.raw.bensoundjazzcomedy;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Psychedelic";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.psychedelic;
        song.mp3Resource = R.raw.bensoundpsychedelic;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Sci-Fi";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.scifi;
        song.mp3Resource = R.raw.bensoundscifi;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "The Elevator Bossa Nova";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.theelevatorbossanova;
        song.mp3Resource = R.raw.bensoundtheelevatorbossanova;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Everyday Is Like Christmas";
        song.artistName = "mixkit.co";
        song.imageResource = R.drawable.santa;
        song.mp3Resource = R.raw.mixkiteverydayislikechristmaswithyou902;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Smooth Like Jazz";
        song.artistName = "mixkit.co";
        song.imageResource = R.drawable.smoothlikejazz;
        song.mp3Resource = R.raw.mixkitsmoothlikejazz24;
        playlist.songs.add(song);
    }

    void connectXMLViews(){
        songsRecyclerView = findViewById(R.id.songList);
        imageView = findViewById(R.id.imageView);
        songNameTextView = findViewById(R.id.songName);
        artistNameTextView = findViewById(R.id.songArtist);
        previousButton = findViewById(R.id.previous);
        pauseButton = findViewById(R.id.pause);
        playButton = findViewById(R.id.play);
        nextButton = findViewById(R.id.next);
    }

    void setupRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);
        songAdapter = new SongAdapter(this, playlist.songs, this);
        songsRecyclerView.setAdapter(songAdapter);
    }

    void displayCurrentSong(){
        Song currentSong = playlist.songs.get(currentSongIndex);
        imageView.setImageResource(currentSong.imageResource);
        songNameTextView.setText(currentSong.songName);
        artistNameTextView.setText(currentSong.artistName);
    }

    void setupButtonHandlers(){
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentSongIndex - 1 >= 0){
                    switchSong(currentSongIndex, currentSongIndex - 1);
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentSongIndex + 1 < playlist.songs.size()){
                    switchSong(currentSongIndex, currentSongIndex + 1);
                }
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playCurrentSong();
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseCurrentSong();
            }
        });
    }

    void switchSong(int fromIndex, int toIndex){
        songAdapter.notifyItemChanged(currentSongIndex);
        currentSongIndex = toIndex;
        displayCurrentSong();
        songAdapter.notifyItemChanged(currentSongIndex);
        songsRecyclerView.scrollToPosition(currentSongIndex);
        if (mediaPlayer != null && mediaPlayer.isPlaying()){
            pauseCurrentSong();
            mediaPlayer = null;
            playCurrentSong();
        }
        else{
            mediaPlayer = null;
        }
    }

    void pauseCurrentSong(){
        if (mediaPlayer != null){
            mediaPlayer.pause();
        }
    }

    void playCurrentSong(){
        if (mediaPlayer == null){
            Song currentSong = playlist.songs.get(currentSongIndex);
            mediaPlayer = MediaPlayer.create(this, currentSong.mp3Resource);
        }

        mediaPlayer.start();
    }

    Playlist playlist = new Playlist();
    SongAdapter songAdapter;
    Integer currentSongIndex = 0;
    MediaPlayer mediaPlayer = null;
    RecyclerView songsRecyclerView;
    ImageView imageView;
    TextView songNameTextView;
    TextView artistNameTextView;
    ImageButton previousButton;
    ImageButton pauseButton;
    ImageButton playButton;
    ImageButton nextButton;
}